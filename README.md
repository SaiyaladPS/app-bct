# android_app-BCT
> - ສະແດງວິດີໂອ
> - Connect database mysql
> - ເຄືອງຄິດເລກ
> - ໂມງ
> - ບາໂຄດ
> - ຄິວອາໂຄດ
> - ຈັບເວລາ
> - ວັນທີ
> - WebView

# image App
<div align="center">
<img src="imageapp/Loginpage.png" width="200px"/>
<img src="imageapp/home_page.png" width="200px"/>
<img src="imageapp/qr and barcode.png" width="200px"/>
<img src="imageapp/void.png" width="200px"/>
</div>

# install
> - android studio https://developer.android.com/studio
> - XAMPP https://www.apachefriends.org/download.html

# fonts
> - NotoSanslao

# database File
> - android.sql
```sql
CREATE TABLE android_table (
  id int(5) NOT NULL,
  fname varchar(255) NOT NULL,
  lname varchar(255) DEFAULT NULL,
  username varchar(255) DEFAULT NULL,
  password varchar(255) DEFAULT NULL,
  gander varchar(255) DEFAULT NULL,
  dob date DEFAULT NULL,
  status varchar(255) DEFAULT NULL,
  tel varchar(255) DEFAULT NULL,
  remerk varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
```
# password and userLogin status Admin
> - <img src="https://www.svgrepo.com/show/286578/users-young.svg" width="20px"/> username : mona
> - <img src="https://www.svgrepo.com/show/284840/password.svg" width="20px"/>  password: 123

# password and userLogin status User

> - <img src="https://www.svgrepo.com/show/286578/users-young.svg" width="20px"/> username : jojo
> - <img src="https://www.svgrepo.com/show/284840/password.svg" width="20px"/>  password: 123

# Tools

<div align="center">
<img src="https://www.svgrepo.com/show/303654/java-logo.svg" width="90px"/>
<img src="https://www.svgrepo.com/show/354575/xampp.svg" width="90px"/>
<img src="https://www.svgrepo.com/show/305701/androidstudio.svg" width="90px"/>
<img src="https://www.svgrepo.com/show/303251/mysql-logo.svg" width="90px"/>
</div>
