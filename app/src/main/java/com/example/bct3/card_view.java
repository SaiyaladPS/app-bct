package com.example.bct3;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class card_view extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_view);

        // ເອີ້ນເອົາໄອດີ
        CardView card_view = findViewById(R.id.home);
        CardView layout1 = findViewById(R.id.video);
        CardView layout2 = findViewById(R.id.caculator);
        CardView cardView = findViewById(R.id.out);
        CardView cardView1 = findViewById(R.id.time);
        CardView cardView2 = findViewById(R.id.barcode_id);
        CardView cardView3 = findViewById(R.id.scroll);
        CardView cardView4 = findViewById(R.id.logout);
        CardView cardView5 = findViewById(R.id.sarch_google);
        CardView cardView6 = findViewById(R.id.cd_id);
        CardView cardView7 = findViewById(R.id.database);
        CardView cardView8 = findViewById(R.id.insert);
        CardView cardView9 = findViewById(R.id.showdataselect);

//        Select data show table
        cardView9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(card_view.this, reporttable.class);
                startActivity(intent);
            }
        });

//        From Insert
        cardView8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(card_view.this, android_table.class);
                startActivity(intent);
            }
        });

//        dataBase
        cardView7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(card_view.this, connectmysql.class);
                startActivity(intent);
            }
        });


        // ຟາຍຫຼັກ
        card_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(card_view.this,Myactivity.class);
                startActivity(intent);
            }
        });
        // ຟາຍວິດີໂອ
        layout1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(card_view.this,MainActivity.class);
                startActivity(intent);
            }
        });
        // ຟາຍເຄື່ອງຄິດໄລ່ເລກ
        layout2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(card_view.this,mycaculator.class);
                startActivity(intent);
            }
        });
        // ກົດປຸ qr_code
        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(card_view.this,QR_Code.class);
                startActivity(intent);
            }
        });
        // ຟາຍວັນທີ່ແລະເວລາ
        cardView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(card_view.this,date_time.class);
                startActivity(intent);
            }
        });
        // ປຸ່ມ barcode
        cardView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(card_view.this,BarCode_and_QR.class);
                startActivity(intent);
            }
        });
        // ປຸ່ມ scroll
        cardView3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(card_view.this,Scroll_View.class);
                startActivity(intent);
            }
        });
        // logout
        cardView4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(card_view.this, calenbar.class);
                startActivity(intent);
            }
        });
        // webview
        cardView5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(card_view.this,WebView.class);
                startActivity(intent);
            }
        });
        // countdown
        cardView6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(card_view.this,CountDown.class);
                startActivity(intent);
            }
        });
    }

}