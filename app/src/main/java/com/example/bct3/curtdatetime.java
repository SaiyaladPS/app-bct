package com.example.bct3;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import java.text.SimpleDateFormat;

public class curtdatetime extends AppCompatActivity {

    TextView showdatetime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_curtdatetime);


        Thread thread = new Thread(){
            @Override
            public void run() {
                try {
                    while (!interrupted()){
                        Thread.sleep(100);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                // ຮັບເອົາໄອດີ ຈາກຟອມ
                               showdatetime=findViewById(R.id.show);
                                long curdt = System.currentTimeMillis();
                                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy\n hh : mm : ss a");
                                String dtstring = simpleDateFormat.format(curdt);
                                // ເອົາໄປສະແດງຜົນຢູ່ TextView
                                showdatetime.setText(dtstring);

                            }
                        });
                    }

                }catch (InterruptedException e){

                }

            }
        };
        thread.start();
    }
}