package com.example.bct3;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class connectmysql extends AppCompatActivity {

    Button connbtn;
    TextView textshow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connectmysql);

        Toolbar toob = findViewById(R.id.id_toolbar);
        setSupportActionBar(toob);
        getSupportActionBar().setTitle("ກວດສອບຖານຂໍ້ມນ");
        // ປຸ່ມກັບຄືນ
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        connbtn = findViewById(R.id.btngetdata);
        textshow = findViewById(R.id.textdata);
        connbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Task().execute();
            }
        });
    }

    // ເອົາເມນູມາສະແດງ
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.exaple_menu, menu);
        return true;
    }
// todo ພາກສວນກົດປຸ່ມ
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();//ເອີ້ນເອົາໄອດີຂອງເມນູມາ
        if(id == R.id.menu_3){
            AlertDialog.Builder alerDialog = new AlertDialog.Builder(connectmysql.this);
            alerDialog.setTitle("Exit App");
            alerDialog.setMessage("Are you sure you want to exit?");
            alerDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    finishAffinity();
                }
            });
            alerDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    dialog.dismiss();
                }
            });
            alerDialog.show();
        }
        return super.onOptionsItemSelected(item);
    }

    class Task extends AsyncTask<Void, Void,Void> {
        String myversion = "", error = "";

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                Class.forName("com.mysql.jdbc.Driver");
                Connection connection = DriverManager.getConnection("jdbc:mysql://your_hots/your_database","your_username","your_password");
                Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery("SELECT version();");
                while (resultSet.next()) {
                    myversion += resultSet.getString(1);
                    System.out.println(resultSet.getString(1));
                }
            } catch(Exception e) {
                error = e.toString();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void unused) {
            super.onPostExecute(unused);
            textshow.setText(myversion);
            if (error!="") {
                textshow.setText(error);
            }

        }
    }

}

