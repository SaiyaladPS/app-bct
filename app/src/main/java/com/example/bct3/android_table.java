package com.example.bct3;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.mindrot.jbcrypt.BCrypt;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.Calendar;

public class android_table extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    EditText fname;
    EditText lname;
    Button buttonInsert;

    TextView selectdate;

    TextView alsert;

    String gander = "";

    RadioGroup radioGroup;
    RadioButton fm,m;
    String status = "";
    Spinner mysqlspinner;
    EditText username,pass,pass_c,tel;

    DatePickerDialog.OnDateSetListener dateSetListener;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_android_table);
        fname = findViewById(R.id.fname);
         lname = findViewById(R.id.lname);
         buttonInsert = findViewById(R.id.btnInsert);
         alsert = findViewById(R.id.alertdata);
         selectdate = findViewById(R.id.dataselect);
         radioGroup = findViewById(R.id.gandergout);
         fm = findViewById(R.id.fmale);
         m = findViewById(R.id.male);
         mysqlspinner = findViewById(R.id.spinnersatust);

         username = findViewById(R.id.username);
         pass = findViewById(R.id.password);
         pass_c = findViewById(R.id.password_c);
         tel = findViewById(R.id.tel);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.status, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_item);
        mysqlspinner.setAdapter(adapter);
        mysqlspinner.setOnItemSelectedListener(this);

         radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
             @Override
             public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.fmale) {
                    gander = "Female";
                } else if(checkedId == R.id.male) {
                    gander = "Male";
                }
             }
         });


        Calendar calendar = Calendar.getInstance();
        final int year = calendar.get(Calendar.YEAR);
        final int moth = calendar.get(Calendar.MONTH);
        final int day = calendar.get(Calendar.DAY_OF_MONTH);

        selectdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog datePickerDialog = new DatePickerDialog(android_table.this, android.R.style.Theme_Holo_Light_Dialog_MinWidth, dateSetListener, year,moth,day);
                datePickerDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                datePickerDialog.show();
            }
        });

        dateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                month = month + 1;
                String date = year+"/"+month+"/"+day;
                selectdate.setText(date);
            }
        };


        buttonInsert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              if (fname.getText().toString().equals("")) {
                    fname.setError("Fname is redlute");
              } else if (lname.getText().toString().equals("")) {
                    lname.setError("Lname is redlute");
              } else if (gander.toString().equals("")) {
                  Toast.makeText(android_table.this,"gander is reqery",Toast.LENGTH_SHORT).show();
              } else if (selectdate.getText().toString().equals("") || selectdate.getText().toString().equals("Select Date")) {
                    Toast.makeText(android_table.this,"Date is req emapty", Toast.LENGTH_SHORT).show();
              } else if (status.toString().equals("") || status.toString().equals("Select Status")) {
                  Toast.makeText(android_table.this, "Status is require", Toast.LENGTH_SHORT).show();
              } else if (username.getText().toString().equals("")) {
                  Toast.makeText(android_table.this,"User is require", Toast.LENGTH_SHORT).show();
                  username.setError("User is require");
              } else if (tel.getText().toString().equals("")) {
                  Toast.makeText(android_table.this, "Number Phone is Require", Toast.LENGTH_SHORT).show();
                  tel.setError("Number Phone is Require");
              } else if (pass.getText().toString().equals("")) {
                  Toast.makeText(android_table.this, "Password is Require", Toast.LENGTH_SHORT).show();
                  pass.setError("Password is Require");
              } else if (pass_c.getText().toString().equals("")) {
                  Toast.makeText(android_table.this, "Comfirem Passowrd is Require", Toast.LENGTH_SHORT).show();
                  pass_c.setError("Comfie Password is Require");
              } else if (!pass.getText().toString().equals(pass_c.getText().toString())) {
                  Toast.makeText(android_table.this,"Password Not Ment", Toast.LENGTH_SHORT).show();
              } else {
                  new Task().execute();
              }
            }
        });
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        status = parent.getItemAtPosition(position).toString();
        Toast.makeText(parent.getContext(),status,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    class Task extends AsyncTask<String, String, String> {
        String error = "";
        String fn = fname.getText().toString();
        String ln = lname.getText().toString();
        String dob = selectdate.getText().toString();
        String use = username.getText().toString();
        String pa = pass.getText().toString();
        String nu = tel.getText().toString();
        @Override
        protected String doInBackground(String... strings) {
            try {
                Class.forName("com.mysql.jdbc.Driver");
                Connection connection = DriverManager.getConnection("jdbc:mysql://your_hots/your_database","your_username","your_password");
                String sql = "INSERT INTO android_table (fname, lname,username,password,gander,dob,status,tel,remerk) VALUE('"+fn+"','"+ln+"','"+use+"',password('"+pa+"'),'"+gander+"','"+dob+"','"+status+"','"+nu+"','')";
                Statement statement = connection.createStatement();
                statement.execute(sql);
            } catch (Exception e) {
                error = e.toString();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (error!="") {
                alsert.setText(error);
            } else  {
                alsert.setText("Insert Success fullry");
                fname.setText("");
                lname.setText("");
                selectdate.setText("Select Date");
                username.setText("");
                pass.setText("");
                pass_c.setText("");
                tel.setText("");
                Toast.makeText(android_table.this, "Insert Success", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(android_table.this, reporttable.class);
                startActivity(intent);
            }
        }
    }
}