package com.example.bct3;

import androidx.activity.result.ActivityResultLauncher;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;

import com.journeyapps.barcodescanner.ScanContract;
import com.journeyapps.barcodescanner.ScanOptions;

public class BarCode_and_QR extends AppCompatActivity {
    TextView resCode ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bar_code_and_qr);

        resCode = findViewById(R.id.resbarcode);
        Button btn_scan = findViewById(R.id.id_barcode);
        btn_scan.setOnClickListener(v -> {
            scanCode();
        });

        Toolbar toob = findViewById(R.id.id_toolbar);
        setSupportActionBar(toob);
        getSupportActionBar().setTitle("ສະແດນ ຄິວອາໂຄດ");
        // ປຸ່ມກັບຄືນ
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void scanCode() {
        ScanOptions options = new ScanOptions();
        options.setPrompt("Valume up flash on");
        options.setBeepEnabled(true);
        options.setOrientationLocked(true);
        options.setCaptureActivity(CapTureAct.class);
        b.launch(options);
    }

    ActivityResultLauncher<ScanOptions> b = registerForActivityResult(new ScanContract(), result -> {
        if (result.getContents() != null) {
            resCode.setText(result.getContents().toString());
            AlertDialog.Builder builder = new AlertDialog.Builder(BarCode_and_QR.this);
            builder.setTitle("Result");
            builder.setMessage(result.getContents());
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            }).show();
        }
    });

    // ເອົາເມນູມາສະແດງ
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.exaple_menu, menu);
        return true;
    }


    // ພາກສ່ວນການກົດປຸ່ມເມນູ

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();//ເອີ້ນເອົາໄອດີຂອງເມນູມາ
        if(id == R.id.menu_3){
            AlertDialog.Builder alerDialog = new AlertDialog.Builder(BarCode_and_QR.this);
            alerDialog.setTitle("Exit App");
            alerDialog.setMessage("Are you sure you want to exit?");
            alerDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    finishAffinity();
                }
            });
            alerDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    dialog.dismiss();
                }
            });
            alerDialog.show();
        }
        return super.onOptionsItemSelected(item);
    }

}