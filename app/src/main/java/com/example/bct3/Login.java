package com.example.bct3;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.mindrot.jbcrypt.BCrypt;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class Login extends AppCompatActivity {

    TextView username,password,res;
    Button btnLogin;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        username = findViewById(R.id.usernamelogin);
        password = findViewById(R.id.passwordlogin);
        btnLogin = findViewById(R.id.btnlogin);
        res = findViewById(R.id.loginalsert);


        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (username.getText().toString().equals("")) {
                    username.setError("Username is required");
                } else if (password.getText().toString().equals("")) {
                    password.setError("Password is required");
                } else {
                    new Task().execute();
                }
            }
        });
    }

    class Task extends AsyncTask<String, String, String>{
        String userLogin = username.getText().toString();
        String passLogin = password.getText().toString();
        String error = "";


        @Override
        protected String doInBackground(String... strings) {
            try {
                Class.forName("com.mysql.jdbc.Driver");
                Connection connection = DriverManager.getConnection("jdbc:mysql://your_host/your_databasenaem","your_user","your_password");
                String sql = "select * from android_table where username = '"+userLogin+"' and password = password('"+passLogin+"')";
                Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery(sql);

                if (resultSet.next()) {
                        if (resultSet.getString("status").toString().equals("admin")) {
                            Intent intent = new Intent(Login.this, card_view.class);
                            startActivity(intent);
                        } else if (resultSet.getString("status").toString().equals("users")) {
                            Intent intent = new Intent(Login.this, card_view.class);
                            startActivity(intent);
                        }
                } else {
                    res.setText("username or password is required");
                }
            } catch (Exception e) {
                error = "username or password is required";
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (error != "") {
                res.setText(error);
            } else {
                username.setText("");
                password.setText("");
                Toast.makeText(Login.this,"Login Success", Toast.LENGTH_SHORT).show();
            }
        }
    }
}