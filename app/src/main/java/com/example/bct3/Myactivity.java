package com.example.bct3;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

public class Myactivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_myactivity);
        // ຮັບເອົາໄອດີຂອງປຸ່ມ NEXT
        Button button = findViewById(R.id.button);
        Button button1 = findViewById(R.id.btn_3);
        Button button2 = findViewById(R.id.text_id);

        // ເວລາກົດປຸ່ມແລ້ວໃຫ້ເກີດມີການທຳງານໃດໜຶ່ງ
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // ເຫດການທີ່ໃຫ້ເກີດຂື້ນ ເວລາມີການກົດປຸ່ມ NEXT
                Intent intent = new Intent(Myactivity.this,MainActivity.class);
                startActivity(intent);
            }
        });
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Myactivity.this,mycaculator.class);
                startActivity(intent);
            }
        });
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Myactivity.this,TextView.class);
                startActivity(intent);
            }
        });

        Toolbar toolbar = findViewById(R.id.idd);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("My Home App");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    // ເອົາເມນຼມາສະແດງ

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.exaple_menu,menu);
        return true;
    }
    // ພາກສ່ວນການກົດປຸ່ມເມນູ

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();//ເອີ້ນເອົາໄອດີຂອງເມນູມາ
        if(id == R.id.menu_3){
            AlertDialog.Builder alerDialog = new AlertDialog.Builder(Myactivity.this);
            alerDialog.setTitle("Exit App");
            alerDialog.setMessage("Are you sure you want to exit?");
            alerDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    finishAffinity();
                }
            });
            alerDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    dialog.dismiss();
                }
            });
            alerDialog.show();
        }
        return super.onOptionsItemSelected(item);
    }
}