package com.example.bct3;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class date_time extends AppCompatActivity {

    TextView showdatetime;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_date_time);

        Thread thread = new Thread(){
            @Override
            public void run() {
                try {
                    while (!interrupted()){
                        Thread.sleep(100);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                // ຮັບເອົາໄອດີ ຈາກຟອມ
                                showdatetime=findViewById(R.id.show);
                                long curdt = System.currentTimeMillis();
                                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy\n hh : mm : ss a");
                                String dtstring = simpleDateFormat.format(curdt);
                                // ເອົາໄປສະແດງຜົນຢູ່ TextView
                                showdatetime.setText(dtstring);

                            }
                        });
                    }

                }catch (InterruptedException e){

                }

            }
        };
        thread.start();

        // set ເວລາ
        setTitle("Date & time");

        Calendar calendar = Calendar.getInstance();
        String currentDate = DateFormat.getDateInstance(android.icu.text.DateFormat.FULL).format(calendar.getTime());
        TextView textView = findViewById(R.id.date);
        textView.setText(currentDate);

        // ປຸ່ມຍ້ອນກັລ
        Toolbar toolbar = findViewById(R.id.id_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("My Date & Time");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }
    // ເອົາເມນຼມາສະແດງ

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.exaple_menu,menu);
        return true;
    }
    // ພາກສ່ວນການກົດປຸ່ມເມນູ

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();//ເອີ້ນເອົາໄອດີຂອງເມນູມາ
        if(id == R.id.menu_3){
            AlertDialog.Builder alerDialog = new AlertDialog.Builder(date_time.this);
            alerDialog.setTitle("Exit App");
            alerDialog.setMessage("Are you sure you want to exit?");
            alerDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    finishAffinity();
                }
            });
            alerDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    dialog.dismiss();
                }
            });
            alerDialog.show();
        }
        return super.onOptionsItemSelected(item);
    }
}