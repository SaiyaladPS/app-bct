package com.example.bct3;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class mycaculator extends AppCompatActivity {

    TextView result;
    EditText box1,box2;
    Button sumbit,minus,khou,han,kam;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mycaculator);

        result = findViewById(R.id.result);
        box1 = findViewById(R.id.input_1);
        box2 = findViewById(R.id.input_2);
        sumbit = findViewById(R.id.plus);
        minus = findViewById(R.id.minus);
        khou = findViewById(R.id.khou);
        han = findViewById(R.id.han);
        kam = findViewById(R.id.kam);

        // ເວລາກົດປຸ່ມ submit ແລ້ວໃຫ້ມີການບວກເລກ
        sumbit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(box1.getText().toString().equals("") && box2.getText().toString().equals("")){
                    Toast.makeText(getApplicationContext(),"Enter value please",Toast.LENGTH_SHORT).show();
                } else if (box1.getText().toString().equals("")) {
                    Toast.makeText(getApplicationContext(),"Enter value1 please",Toast.LENGTH_SHORT).show();
                } else if (box2.getText().toString().equals("")) {
                    Toast.makeText(getApplicationContext(),"Enter value2 please",Toast.LENGTH_SHORT).show();
                }else{
                    // ປະກາດຕົວປ່ຽນໃໝ່ ເພື່ອໃຫ້ເອົາຕົວເລກທີ່ປ້ອນຈາກຟອມ

                    double num1 = Double.parseDouble(box1.getText().toString());
                    double num2 = Double.parseDouble(box2.getText().toString());
                    double mysum = num1 + num2;
                    // ປ່ຽນເອົາຜົນບວກໃຫ້ເປັນຕົວໜັງສື ເພື່ອເອົາໄປສະແດງຜົນ
                    String myshow = String.valueOf(mysum);
                    // ຄຳສັ່ງເອົາຜົນບວກໄປສະແດງຢູ່ Result ເວລາກົດປຸ່ມ submit ຢູ່ໃນຟອມ
                    result.setText(myshow);
                }
            }
        });
        minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(box1.getText().toString().equals("") && box2.getText().toString().equals("")){
                    Toast.makeText(getApplicationContext(),"Enter value please",Toast.LENGTH_SHORT).show();
                } else if (box1.getText().toString().equals("")) {
                    Toast.makeText(getApplicationContext(),"Enter value1 please",Toast.LENGTH_SHORT).show();
                } else if (box2.getText().toString().equals("")) {
                    Toast.makeText(getApplicationContext(),"Enter value2 please",Toast.LENGTH_SHORT).show();
                }else{
                    // ປະກາດຕົວປ່ຽນໃໝ່ ເພື່ອໃຫ້ເອົາຕົວເລກທີ່ປ້ອນຈາກຟອມ

                    double num1 = Double.parseDouble(box1.getText().toString());
                    double num2 = Double.parseDouble(box2.getText().toString());
                    double mysum = num1 - num2;
                    // ປ່ຽນເອົາຜົນບວກໃຫ້ເປັນຕົວໜັງສື ເພື່ອເອົາໄປສະແດງຜົນ
                    String myshow = String.valueOf(mysum);
                    // ຄຳສັ່ງເອົາຜົນບວກໄປສະແດງຢູ່ Result ເວລາກົດປຸ່ມ submit ຢູ່ໃນຟອມ
                    result.setText(myshow);
                }
            }
        });
        khou.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(box1.getText().toString().equals("") && box2.getText().toString().equals("")){
                    Toast.makeText(getApplicationContext(),"Enter value please",Toast.LENGTH_SHORT).show();
                } else if (box1.getText().toString().equals("")) {
                    Toast.makeText(getApplicationContext(),"Enter value1 please",Toast.LENGTH_SHORT).show();
                } else if (box2.getText().toString().equals("")) {
                    Toast.makeText(getApplicationContext(),"Enter value2 please",Toast.LENGTH_SHORT).show();
                }else{
                    // ປະກາດຕົວປ່ຽນໃໝ່ ເພື່ອໃຫ້ເອົາຕົວເລກທີ່ປ້ອນຈາກຟອມ

                    double num1 = Double.parseDouble(box1.getText().toString());
                    double num2 = Double.parseDouble(box2.getText().toString());
                    double mysum = num1 * num2;
                    // ປ່ຽນເອົາຜົນບວກໃຫ້ເປັນຕົວໜັງສື ເພື່ອເອົາໄປສະແດງຜົນ
                    String myshow = String.valueOf(mysum);
                    // ຄຳສັ່ງເອົາຜົນບວກໄປສະແດງຢູ່ Result ເວລາກົດປຸ່ມ submit ຢູ່ໃນຟອມ
                    result.setText(myshow);
                }
            }
        });
        han.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(box1.getText().toString().equals("") && box2.getText().toString().equals("")){
                    Toast.makeText(getApplicationContext(),"Enter value please",Toast.LENGTH_SHORT).show();
                } else if (box1.getText().toString().equals("")) {
                    Toast.makeText(getApplicationContext(),"Enter value1 please",Toast.LENGTH_SHORT).show();
                } else if (box2.getText().toString().equals("")) {
                    Toast.makeText(getApplicationContext(),"Enter value2 please",Toast.LENGTH_SHORT).show();
                }else{
                    // ປະກາດຕົວປ່ຽນໃໝ່ ເພື່ອໃຫ້ເອົາຕົວເລກທີ່ປ້ອນຈາກຟອມ

                    double num1 = Double.parseDouble(box1.getText().toString());
                    double num2 = Double.parseDouble(box2.getText().toString());
                    double mysum = num1 / num2;
                    // ປ່ຽນເອົາຜົນບວກໃຫ້ເປັນຕົວໜັງສື ເພື່ອເອົາໄປສະແດງຜົນ
                    String myshow = String.valueOf(mysum);
                    // ຄຳສັ່ງເອົາຜົນບວກໄປສະແດງຢູ່ Result ເວລາກົດປຸ່ມ submit ຢູ່ໃນຟອມ
                    result.setText(myshow);
                }
            }
        });
        kam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(box1.getText().toString().equals("") && box2.getText().toString().equals("")){
                    Toast.makeText(getApplicationContext(),"Enter value please",Toast.LENGTH_SHORT).show();
                } else if (box1.getText().toString().equals("")) {
                    Toast.makeText(getApplicationContext(),"Enter value1 please",Toast.LENGTH_SHORT).show();
                } else if (box2.getText().toString().equals("")) {
                    Toast.makeText(getApplicationContext(),"Enter value2 please",Toast.LENGTH_SHORT).show();
                }else{
                    // ປະກາດຕົວປ່ຽນໃໝ່ ເພື່ອໃຫ້ເອົາຕົວເລກທີ່ປ້ອນຈາກຟອມ

                    double num1 = Double.parseDouble(box1.getText().toString());
                    double num2 = Double.parseDouble(box2.getText().toString());
                    double mysum = Math.pow(num1,num2);
                    // ປ່ຽນເອົາຜົນບວກໃຫ້ເປັນຕົວໜັງສື ເພື່ອເອົາໄປສະແດງຜົນ
                    String myshow = String.valueOf(mysum);
                    // ຄຳສັ່ງເອົາຜົນບວກໄປສະແດງຢູ່ Result ເວລາກົດປຸ່ມ submit ຢູ່ໃນຟອມ
                    result.setText(myshow);

                }
            }
        });

        // ປຸ່ມຍ້ອນກັລ
        Toolbar toolbar = findViewById(R.id.id_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("My caculator");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    // ເອົາເມນຼມາສະແດງ

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.exaple_menu,menu);
        return true;
    }
    // ພາກສ່ວນການກົດປຸ່ມເມນູ

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();//ເອີ້ນເອົາໄອດີຂອງເມນູມາ
        if(id == R.id.menu_3){
            AlertDialog.Builder alerDialog = new AlertDialog.Builder(mycaculator.this);
            alerDialog.setTitle("Exit App");
            alerDialog.setMessage("Are you sure you want to exit?");
            alerDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    finishAffinity();
                }
            });
            alerDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    dialog.dismiss();
                }
            });
            alerDialog.show();
        }
        return super.onOptionsItemSelected(item);
    }


}