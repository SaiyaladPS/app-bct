package com.example.bct3;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class reporttable extends AppCompatActivity {

    Button btnselect;

    TextView tvfname;
    TextView tvlname;
    TextView tvdob;
    TextView tvtel;
    TextView tvid;

    TextView res;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reporttable);

        btnselect = findViewById(R.id.btnSelect);
        tvid = findViewById(R.id.tableid);
        tvfname = findViewById(R.id.tablefname);
        tvlname = findViewById(R.id.tablelname);
        tvdob = findViewById(R.id.tabledob);
        tvtel = findViewById(R.id.tabletel);
        res = findViewById(R.id.showalert);

        Toolbar toob = findViewById(R.id.id_toolbar);
        setSupportActionBar(toob);
        getSupportActionBar().setTitle("ສະແດນ ຄິວອາໂຄດ");
        // ປຸ່ມກັບຄືນ
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        btnselect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Task().execute();
            }
        });
    }

    class Task extends AsyncTask<Void,Void,Void> {
        String error;
        String id ="",fname="",lname="",dob="",tel="";

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                Class.forName("com.mysql.jdbc.Driver");
                Connection connection = DriverManager.getConnection("jdbc:mysql://your_hots/your_database","your_username","your_password");
                Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery("select * from android_table");
                while (resultSet.next()) {
                    id += resultSet.getString(1)+"\n";
                    fname += resultSet.getString(2)+"\n";
                    lname += resultSet.getString(3)+"\n";
                    dob += resultSet.getString(7)+"\n";
                    tel += resultSet.getString(9)+"\n";
                }
            } catch (Exception e) {
                error = e.toString();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void unused) {
            super.onPostExecute(unused);
            tvid.setText(id);
            tvfname.setText(fname);
            tvlname.setText(lname);
            tvdob.setText(dob);
            tvtel.setText(tel);

            if (error!= "") {
                res.setText(error);
            } else {
                res.setText("select success");
            }
        }
    }

    // ເອົາເມນູມາສະແດງ
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.exaple_menu, menu);
        return true;
    }

    // ພາກສ່ວນການກົດປຸ່ມເມນູ

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();//ເອີ້ນເອົາໄອດີຂອງເມນູມາ
        if(id == R.id.menu_3){
            AlertDialog.Builder alerDialog = new AlertDialog.Builder(reporttable.this);
            alerDialog.setTitle("Exit App");
            alerDialog.setMessage("Are you sure you want to exit?");
            alerDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    finishAffinity();
                }
            });
            alerDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    dialog.dismiss();
                }
            });
            alerDialog.show();
        }
        return super.onOptionsItemSelected(item);
    }
}